import sqlite from 'sqlite'
import SQL from 'sql-template-strings';

const initializeDatabase = async () => {

  const db = await sqlite.open('./db.sqlite');
  
  const createCandle = async (props) => {
    if(!props || !props.name || !props.per ||!props.smell){
      throw new Error(`you must provide everything`)
    }
    const { name, per, smell } = props
    try{
      const result = await db.run(SQL`INSERT INTO candles (name,per,smell) VALUES (${name}, ${per},${smell})`);
      const id = result.stmt.lastID
      return id
    }catch(e){
      throw new Error(`couldn't insert this combination: `+e.message)
    }
  }
  
  /**
   * deletes a contact
   * @param {number} id the id of the contact to delete
   * @returns {boolean} `true` if the contact was deleted, an error otherwise 
   */
  const deleteCandle = async (id) => {
    try{
      const result = await db.run(SQL`DELETE FROM candles WHERE candle_id = ${id}`);
      if(result.stmt.changes === 0){
        throw new Error(`candle "${id}" does not exist`)
      }
      return true
    }catch(e){
      throw new Error(`couldn't delete the candle "${id}": `+e.message)
    }
  }
  
 /**
   * Edits a contact
   * @param {number} id the id of the contact to edit
   * @param {object} props an object with at least one of ???
   */
  const updateCandle = async (id, props) => {
    if (!props || !(props.name || props.per || props.smell)) {
      throw new Error(`you must provide a name`);
    }
    const { name, per, smell } = props;
    try {
      let statement = "";
      if (name && per && smell) {
        statement = SQL`UPDATE candles SET per=${per}, name=${name},smell=${smell} WHERE candle_id = ${id}`;
      } else if (name) {
        statement = SQL`UPDATE candle SET name=${name} WHERE candle_id = ${id}`;
      } else if (per) {
        statement = SQL`UPDATE candle SET per=${per} WHERE candle_id = ${id}`;
      } else if (smell){
        statement = SQL`UPDATE candle SET smell=${smell} WHERE candle_id = ${id}`;
      }
      const result = await db.run(statement);
      if (result.stmt.changes === 0) {
        throw new Error(`no changes were made`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't update the candle ${id}: ` + e.message);
    }
  }
  
  /**
   * Retrieves a contact
   * @param {number} id the id of the contact
   * @returns {object} an object with `name`, `email`, and `id`, representing a contact, or an error 
   */
  const getCandle = async (id) => {
    try{
      const candlesList = await db.all(SQL`SELECT candle_id AS id, name, per,smell FROM candles WHERE candle_id = ${id}`);
      const candle = candlesList[0]
      if(!candle){
        throw new Error(`candle ${id} not found`)
      }
      return candle
    }catch(e){
      throw new Error(`couldn't get the candle ${id}: `+e.message)
    }
  }
  
  /**
   * retrieves the contacts from the database
   * @param {string} orderBy an optional string that is either "name" or "email"
   * @returns {array} the list of contacts
   */
  const getCandlesList = async (orderBy) => {
    try{
      
      let statement = `SELECT candle_id AS id, name, per, smell FROM candles`
      switch(orderBy){
        case 'name': statement+= ` ORDER BY name`; break;
        case 'per': statement+= ` ORDER BY per`; break;
        case 'smell': statement+= ` ORDER BY smell`; break;
        default: break;
      }
      const rows = await db.all(statement)
      if(!rows.length){
        throw new Error(`no rows found`)
      }
      return rows
    }catch(e){
      throw new Error(`couldn't retrieve candles: `+e.message)
    }
  }
  
  const controller = {
    createCandle,
    deleteCandle,
    updateCandle,
    getCandle,
    getCandlesList
  }

  return controller
}

export default initializeDatabase
